from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import RedirectView

from apps.exam.urls import exam_urlpatterns


urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/exam')),
    url(r'^exam/', include(exam_urlpatterns, namespace='exam', app_name='exam')),
    url(r'^admin/', include(admin.site.urls)),
    url('^', include('django.contrib.auth.urls'))
]
