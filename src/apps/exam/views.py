from django.views.generic.base import TemplateView
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.template.loader import render_to_string

from .models import TestItem, TestResult


class IndexView(TemplateView):
    template_name = 'tests_index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['tests_list'] = TestItem.objects.all_with_passed_count()
        return context
index_view = login_required(IndexView.as_view(), redirect_field_name='')


class TestView(TemplateView):
    template_name = 'inc/test_form.html'

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        tid = self.kwargs['test_id']
        test = TestItem.objects.get(pk=tid).to_dict()
        context['test'] = test
        return context

    def _offline(self):
        """Download test template to take it off-line
        :return:
        """
        context = self.get_context_data()
        tid = self.kwargs['test_id']
        test = TestItem.objects.get(pk=tid)
        context['right_answers'] = test.right_answers_ids_list()
        test_template = ContentFile(render_to_string('inc/test_offline.html', context))
        response = HttpResponse(test_template, 'application/html')
        response['Content-Length'] = test_template.size
        response['Content-Disposition'] = 'attachment; filename="test.html"'
        return response

    def get(self, request, *args, **kwargs):
        if self.kwargs.get('offline'):
            return self._offline()
        if request.is_ajax() and not request.GET.get('as_html') == '1':
            return JsonResponse(self.get_context_data()['test'])
        return super(TestView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        tid = self.kwargs['test_id']
        data = request.POST.copy()
        answers = []
        for k in data:
            if k.startswith('q_'):
                answers.extend(data.getlist(k))
        answers = map(int, answers)
        answers.sort()

        test, created = TestResult.objects.get_or_create(
            user=request.user, test_id=tid)
        test.complete(answers)
        result = test.check_result(save=True)
        if request.is_ajax():
            return JsonResponse({'status': result})
        return super(TestView, self).post(request, *args, **kwargs)
test_view = login_required(TestView.as_view(), redirect_field_name='')
