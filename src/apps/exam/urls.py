from django.conf.urls import patterns, url


exam_urlpatterns = patterns(
    'src.apps.exam.views',
    url(r'^test/(?P<test_id>\d+)$', 'test_view', name='test'),
    url(r'^test/(?P<test_id>\d+)/offline$', 'test_view', name='test-offline',
        kwargs={'offline': True}),
    url(r'^$', 'index_view'),
)
