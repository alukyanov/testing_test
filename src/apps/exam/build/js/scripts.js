(function($){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function buildTestInfo(data){
        var testInfo = $('#test-info');
        testInfo.html(data);
        testInfo.addClass('col-md-9').removeClass('hidden');
        testInfo.find('#test-offline').on('click', function(){

        });
        testInfo.find('[type="submit"]').on('click', function(){
            var form = testInfo.find('form'),
                data = form.serializeObject();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: data,
                success: function(rs){
                    var res = rs.status == true
                        ? 'Тест пройден успешно'
                        : 'Тест провалился';
                    form.find('#test-result').html(res);
                }
            });
            return false;
        });
    }

    $('#tests-list button').on('click', function(){
        var tid = $(this).data('tid');
        $.ajax({
            url: '/exam/test/' + tid + '?as_html=1',
            success: function(rs){
                buildTestInfo(rs);
            }
        })
    });
})(jQuery);