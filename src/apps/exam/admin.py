# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import TestItem, QuestionItem, AnswerItem, TestQuestions, \
    QuestionAnswers, TestResult


class HiddenModelAdmin(admin.ModelAdmin):
    def get_model_perms(self, *args, **kwargs):
        perms = admin.ModelAdmin.get_model_perms(self, *args, **kwargs)
        perms['list_hide'] = True
        return perms


class AnswerInline(admin.TabularInline):
    model = QuestionAnswers


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [AnswerInline]


class QuestionInline(admin.StackedInline):
    model = TestQuestions


class TestAdmin(admin.ModelAdmin):
    list_display = ('name', )
    list_display_links = ('name', )
    search_fields = ('name', )
    inlines = [QuestionInline]


class TestResultAdmin(admin.ModelAdmin):
    list_display = ('user', 'test', 'started', 'completed', 'is_passed')


admin.site.register(TestItem, TestAdmin)
admin.site.register(QuestionItem, QuestionAdmin)
admin.site.register(AnswerItem)
admin.site.register(TestResult)
