import os
from django_assets import Bundle, register
from django.conf import settings


BUILD_DIR = os.path.join(settings.BASE_DIR, 'apps/exam/build')


js = Bundle(
    os.path.join(BUILD_DIR, 'js/scripts.js'),
    filters='jsmin',
    output='exam/js/exam.min.js'
)
css = Bundle(
    os.path.join(BUILD_DIR, 'css/main.css'),
    filters='cssmin',
    output='exam/css/css.min.css'
)


register('js_exam', js)
register('css_exam', css)
