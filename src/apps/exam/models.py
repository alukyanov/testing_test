# -*- coding: utf-8 -*-
import json
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
# from django.contrib.postgres.fields import JSONField # in Django 1.9


class NamedItem(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name


class AnswerItem(NamedItem):
    class Meta:
        verbose_name = u"Ответ"
        verbose_name_plural = u"Ответы"


class QuestionItem(NamedItem):
    answers = models.ManyToManyField(AnswerItem, through='QuestionAnswers')

    class Meta:
        verbose_name = u"Вопрос"
        verbose_name_plural = u"Вопросы"

    def to_dict(self, with_right_answers=None):
        """Returns a serialized question
        :param with_right_answers:
        :return:
        """
        ra = lambda x: {'is_right': x.answer.is_right} \
            if with_right_answers else {}
        answers = QuestionAnswers.objects.filter(question=self)
        return dict(
            pk=self.pk,
            name=self.name,
            answers=[dict(pk=a.pk, name=unicode(a.answer), **ra(a))
                     for a in answers]
        )


class QuestionAnswers(models.Model):
    question = models.ForeignKey(QuestionItem, on_delete=models.CASCADE)
    answer = models.ForeignKey(AnswerItem, on_delete=models.CASCADE)
    is_right = models.BooleanField(u"Правильный ответ", default=False)


class TestItemManager(models.Manager):
    def all_with_passed_count(self):
        tests_list = self.all()
        passed_list = TestResult.objects.values('test').annotate(
            passed_count=models.Count('test')
        ).order_by().values_list('test_id', 'passed_count')
        for test in tests_list:
            test.passed_count = dict(passed_list).get(test.id, 0)
        return tests_list


class TestItem(NamedItem):
    questions = models.ManyToManyField(QuestionItem, through='TestQuestions')

    objects = TestItemManager()

    class Meta:
        verbose_name = u"Тест"
        verbose_name_plural = u"Тесты"

    def to_dict(self, with_right_answers=None):
        return dict(
            pk=self.pk,
            name=self.name,
            questions=[q.to_dict(with_right_answers)
                       for q in self.questions.select_related()]
        )

    def right_answers_ids_list(self):
        questions_ids_list = self.questions.values_list('id', flat=True)
        answers_ids_list = list(QuestionAnswers.objects.filter(
            is_right=True, question__id__in=questions_ids_list
        ).values_list('id', flat=True))
        answers_ids_list.sort()
        return answers_ids_list


class TestQuestions(models.Model):
    test = models.ForeignKey(TestItem, on_delete=models.CASCADE)
    question = models.ForeignKey(QuestionItem, on_delete=models.CASCADE)
    ordering = models.PositiveSmallIntegerField(
        u"Сортировка", default=0, blank=True)


class TestResult(models.Model):
    user = models.ForeignKey(User)
    test = models.ForeignKey(TestItem, related_name='passed_tests')
    started = models.DateTimeField(
        u'Начало теста', auto_now_add=True, editable=False)
    completed = models.DateTimeField(
        u'Завершение теста', blank=True, null=True)
    is_passed = models.BooleanField(default=False)
    # TODO: use JSONField or ArrayField in Django 1.9
    answers = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = u'Результат тестирования'
        verbose_name_plural = u'Результаты тестирования'
        ordering = ('-started', 'test')

    def __unicode__(self):
        return u'<TestResult: user=\'{0}\', test=\'{1}\', is_passed={2}>' \
               u''.format(self.user, self.test, self.is_passed)

    def complete(self, data):
        self.completed = datetime.now()
        self.set_answers(data)
        self.save()

    def answers_ids_list(self):
        """Workaround JSONField
        :return:
        """
        # TODO: check is the answers field is JSONField
        ids_list = json.loads(self.answers) if self.answers else {}
        ids_list.sort()
        return ids_list

    def set_answers(self, data, save=False):
        """Set answers
        :param data:
        :param save:
        :return:
        """
        if isinstance(data, list):
            self.answers = json.dumps(data)
        elif isinstance(data, basestring) and isinstance(json.loads(data), list):
            self.answers = data
        else:
            return
        if save:
            self.save()

    def check_result(self, save=False):
        """Checks is passed answers are right
        :param save: save result into the DB
        :return:
        """
        right_answers_ids_list = self.test.right_answers_ids_list()
        answers_ids_list = self.answers_ids_list()
        print right_answers_ids_list, answers_ids_list
        result = answers_ids_list == right_answers_ids_list
        if save:
            self.is_passed = result
            self.save()
        return result
